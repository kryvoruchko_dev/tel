const express = require("express");
const cron = require('node-cron');
const mongoose = require("mongoose");
const { parse } = require('rss-to-json');
const TelegramBot = require('node-telegram-bot-api');
const token = '5617712128:AAFGi051REK4t3c4CroNuW3MKSQ8yQIEWds';

const link = 'https://www.upwork.com/ab/feed/topics/rss?securityToken=a2d053f107cf95e5bad3324ce18537b45e5a7aa43b51d4ba7e02222a228f0a26cd4a407fc16e3c33f43ff785bae1d334d274f39ec4cbd080c9d3ed7e4088910d&userUid=1567124587804864512&orgUid=1567124587804864513&topic=most-recent';
mongoose.connect("mongodb+srv://sinclar96:Sinclar999@cluster0.kctlsft.mongodb.net/?retryWrites=true&w=majority", { useNewUrlParser: true, useUnifiedTopology: true });

const app = express();
const Schema = mongoose.Schema;
const jobScheme = new Schema({
    guid: String,
    title: String,
    description: String,
    link: String,
    published: Number,
    created: Number,
    category: Array,
    enclosures: Array,
    content: String,
    content_encoded: String,
    media: Object,
});
const chatScheme = new Schema({
    chatId: String,
});
const Job = mongoose.model("Job", jobScheme);
const Chat = mongoose.model("Chat", chatScheme);

const bot = new TelegramBot(token, {polling: true});

async function saveUser(id) {
    const chats = await Chat.find({});
    const index = chats.findIndex(c => +c.chatId === +id);
    if (index < 0) {
        const newChat = new Chat({ chatId: id });

        newChat.save(function(err){
            mongoose.disconnect();
            if(err) return console.log(err);
            console.log("Chat saved", newChat);
        });
    }
}

function sendMessagesForUsers(job, savedChats) {
    savedChats.forEach(c => {
        bot.sendMessage(
            c.chatId,
            '🔥 🔥 🔥 NEW JOB 🔥 🔥 🔥 \n \n \n' +
            '🟢 <b>' + job.title  + '</b> 🟢 \n \n' +
            job.description.replaceAll('\n', '').replaceAll('<br />', '\n') + '\n',
            { parse_mode: 'HTML'}
        );
    });
}

bot.on('message', (msg) => {
    const chatId = msg.chat.id;
    saveUser(chatId);
});

cron.schedule('* * * * *', function() {
    (async () => {
        const savedJobs = await Job.find({});
        const savedChats = await Chat.find({});
        const rss = await parse(link);
        const text = JSON.stringify(rss, null, 3);
        const data = JSON.parse(text);
        const jobs = data.items;
        let dataSaveJobs = [];

        jobs.forEach(job => {
            const linkItems = job.link.split('_%7');
            const guid = linkItems[1].split('?')[0];
            const findJobIndex = savedJobs.findIndex(savedJob => savedJob.guid && savedJob.guid === guid);

            if (findJobIndex < 0) {
                dataSaveJobs.push({
                    ...job, guid
                });
            }
        });

        const dataSave = dataSaveJobs;

        dataSave.forEach(j => {
            sendMessagesForUsers(j, savedChats);
        });

        Job.insertMany(dataSaveJobs).then(() => {
            console.log("Jobs saved - ", dataSaveJobs.length);
            dataSaveJobs = [];
        });
    })();
});

app.listen(3000);